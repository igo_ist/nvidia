// Для добавления функционала используем классы с префиксом js, стилизовать по этим классам нельзя

$(document).ready(function(){

	// $('input[type=tel]')
	// 	.inputmask("8 (999) 999 99 99");


	// $('.js-popup-img')
	// 	.magnificPopup({
	// 		type:'image',
	// 		closeOnContentClick: true,
	// 		fixedContentPos: true,
	// 		mainClass: 'mfp-no-margins mfp-with-zoom',
	// 		image: {
	// 			verticalFit: true
	// 		},
	// 		zoom: {
	// 			enabled: true,
	// 			duration: 300
	// 		}
	// 	});
	
	$('.js-card__title').each(function(){
		let title = $(this);
		let titleWords = title.text().split(' ').length;
		titleWords < 3 ?
		title.html(title.text().replace(/(^\w+)/,'<span class="card__title_accent">$1</span>')) :
		title.html(title.text().replace(/(^\w+\s\w+)/,'<span class="card__title_accent">$1</span>'));
	});
	$('.js-burger').on('click', function () {
		$('.menu').toggleClass('active')
		$('.header__item').toggleClass('fixed')
		$('.burger__line').toggleClass('active')
	});
});
